package hu.gerifield.brainwaveapp.app;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.neurosky.thinkgear.TGDevice;
import com.neurosky.thinkgear.TGEegPower;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String LOGSTRING = "MyBrainwaveTestApp";
    public static final String HISTORY_LOG_FILE = Environment.getExternalStorageDirectory().getPath() + "/brainwaveHistory_";;

    private TGDevice tgDevice;
    private BluetoothAdapter btAdapter;
    private TextView stateTextView, attentionAvr;
    private EditText activityName;
    private Button connectButton, disconnectButton, startButton, stopButton;
    private String[] deviceStates;
    private String logFileName;

    private double attSum = 0;
    private double attNum = 0;

    private double medSum = 0;
    private double medNum = 0;

    private ProgressBar poorSignalProgressBar, attentionProgressBar, meditationProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stateTextView = (TextView)findViewById(R.id.stateTextView);
        connectButton = (Button)findViewById(R.id.connectButton);
        disconnectButton = (Button)findViewById(R.id.disconnectButton);

        poorSignalProgressBar = (ProgressBar)findViewById(R.id.poorSignalProgressBar);
        attentionProgressBar = (ProgressBar)findViewById(R.id.attentionProgressBar);
        meditationProgressBar = (ProgressBar)findViewById(R.id.meditationProgressBar);

        startButton = (Button) findViewById(R.id.start_button);
        stopButton = (Button) findViewById(R.id.stop_button);

        activityName = (EditText) findViewById(R.id.activityName);

        attentionAvr = (TextView) findViewById(R.id.attention_avr);
        attentionAvr.setMovementMethod(new ScrollingMovementMethod());
        attentionAvr.setText(getBrainwaveHistory());

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter != null){
            tgDevice = new TGDevice(btAdapter, handler);
            //tgDevice.connect(true);
        }

        deviceStates = getResources().getStringArray(R.array.states);

        connectButton.setOnClickListener(this);
        disconnectButton.setOnClickListener(this);

        startButton.setOnClickListener(this);
        stopButton.setOnClickListener(this);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        stateTextView.setText(savedInstanceState.getString("LastState"));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putString("LastState", stateTextView.getText().toString());
    }

    private final android.os.Handler handler = new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what){
                case TGDevice.MSG_STATE_CHANGE:
                    switch (msg.arg1){
                        case TGDevice.STATE_IDLE:
                            Log.i(LOGSTRING, "State IDLE");
                            stateTextView.setText(deviceStates[0]);
                            break;
                        case TGDevice.STATE_CONNECTING:
                            Log.i(LOGSTRING, "State connecting");
                            stateTextView.setText(deviceStates[1]);
                            break;
                        case TGDevice.STATE_CONNECTED:
                            Log.i(LOGSTRING, "State connected");
                            tgDevice.start();
                            stateTextView.setText(deviceStates[2]);
                            break;
                        case TGDevice.STATE_DISCONNECTED:
                            Log.i(LOGSTRING, "State disconnected");
                            stateTextView.setText(deviceStates[3]);
                            break;
                        case TGDevice.STATE_NOT_FOUND:
                        case TGDevice.STATE_NOT_PAIRED:
                        default:
                            Log.i(LOGSTRING, "State other...");
                            stateTextView.setText(deviceStates[4]);
                            break;
                    }
                break;
                case TGDevice.MSG_POOR_SIGNAL:
                    Log.i(LOGSTRING, "Poor signal: "+msg.arg1);
                    poorSignalProgressBar.setProgress(msg.arg1);
                    break;
                case TGDevice.MSG_ATTENTION:
                    Log.i(LOGSTRING, "Attention: "+msg.arg1);
                    attentionProgressBar.setProgress(msg.arg1);
                    double attention = msg.arg1;

                    recordBrainwave("Attention - " + attention);
                    attSum += attention;
                    attNum += 1;

                    break;
                case TGDevice.MSG_MEDITATION:
                    double meditation = msg.arg1;
                    Log.i(LOGSTRING, "Meditation: " + meditation);

                    meditationProgressBar.setProgress(msg.arg1);
                    recordBrainwave("Meditation - " + meditation);
                    medSum += meditation;
                    medNum += 1;

                    break;
                case TGDevice.MSG_RAW_DATA:
                    int rawValue = msg.arg1;
                    Log.v(LOGSTRING, "RAW: "+rawValue);
                    break;
                case TGDevice.MSG_EEG_POWER:
                    TGEegPower ep = (TGEegPower) msg.obj;
                    String textEeg = String.format("EegPower - %s %s %s %s %s %s %s %s", ep.delta, ep.theta, ep.highAlpha, ep.lowAlpha, ep.highBeta, ep.lowBeta, ep.lowGamma, ep.midGamma);
                    Log.v(LOGSTRING, textEeg);

                    recordBrainwave(textEeg);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.connectButton:
                tgDevice.connect(true);
                break;
            case R.id.disconnectButton:
                tgDevice.close();
                //tgDevice.connect(false);
                break;
            case R.id.start_button:
                logFileName = new SimpleDateFormat("ddMMyyyyhhmmss").format(new Date());
                break;
            case R.id.stop_button:
                logFileName = null;

                if ((attNum != 0 || medNum != 0) && tgDevice.getState() == TGDevice.STATE_CONNECTED) {

                    NumberFormat formatter = new DecimalFormat("#0.00");

                    String attentionAverage = "Attention: "+ formatter.format(attSum / attNum);
                    String meditationAverage = "Meditation: "+ formatter.format(medSum / medNum);

                    String activityNameStr = activityName.getText().toString();

                    recordBrainwaveHistory(String.format("%s - %s %s", activityNameStr, attentionAverage, meditationAverage));

                    attentionAvr.setText(getBrainwaveHistory());

                    attSum = 0;
                    medSum = 0;
                    attNum = 0;
                    medNum = 0;
                    activityName.setText("");
                }

                break;
        }
    }

    private String getBrainwaveHistory() {
        if(!new File(HISTORY_LOG_FILE).exists())
            return "";

        String history = "";
        try {
            history = this.readFile(HISTORY_LOG_FILE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return history;
    }

    private void recordBrainwave(String txtData) {
        String brainWavesLogFile = Environment.getExternalStorageDirectory().getPath() + "/brainwaveRecordings_";
        brainWavesLogFile = brainWavesLogFile + logFileName +".txt";
        if(logFileName == null){
            return;
        }
        recordInFile(txtData, brainWavesLogFile);
    }

    private void recordBrainwaveHistory(String txtData) {
        recordInFile(txtData, HISTORY_LOG_FILE);
    }

    private void recordInFile(String txtData, String fileName) {
        try {
            txtData = String.format("%s %s", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.S - ").format(new Date()), txtData);

            File brainwaveLog = new File(fileName);
            FileOutputStream fOut = new FileOutputStream(brainwaveLog, true);

            try {
                OutputStreamWriter ow = new OutputStreamWriter(fOut);
                BufferedWriter bw = new BufferedWriter(ow);

                bw.write(txtData);
                bw.newLine();

                bw.flush();
                ow.flush();

            } finally {
                fOut.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readFile(String pathname) throws IOException {

        File file = new File(pathname);
        List<String> fileContents = new LinkedList<String>();
        Scanner scanner = new Scanner(file);
        String lineSeparator = System.getProperty("line.separator");

        try {
            while(scanner.hasNextLine()) {
                fileContents.add(scanner.nextLine() + lineSeparator);
            }
            Collections.reverse(fileContents);

            String history = "";
            for (String s : fileContents) {
                history += s;
            }

            return history;
        } finally {
            scanner.close();
        }
    }
}
